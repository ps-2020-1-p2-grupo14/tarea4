CC=gcc
INC_DIR = ./include
BIN_DIR = ./bin
OBJ_DIR = ./obj
LIB_DIR = ./lib
SRC_DIR = ./src
OBJS = $(OBJ_DIR)/dias.o \
       $(OBJ_DIR)/segundos.o
EXENAME = utilfecha
CFLAGS= -Wall -I$(INC_DIR)/
LIB = libconv
WE = ar rcs
DIN = -shared -fPIC
EST = -static -o

.PHONY: estatico dinamico debug clean 

all: estatico dinamico

estatico: 
	$(CC) -c $(SRC_DIR)/dias.c -o $(OBJ_DIR)/dias.o $(DFLAGS)
	$(CC) -c $(SRC_DIR)/segundos.c -o $(OBJ_DIR)/segundos.o $(DFLAGS)
	$(WE) $(LIB_DIR)/$(LIB).a $(OBJS)
	$(CC) -c $(CFLAGS) $(SRC_DIR)/main.c -o $(OBJ_DIR)/main.o $(DFLAGS)
	$(CC) $(DFLAGS) $(EST) $(BIN_DIR)/estatico $(OBJ_DIR)/main.o -L$(LIB_DIR) -lconv 

dinamico:
	$(CC) $(DIN) $(SRC_DIR)/dias.c $(SRC_DIR)/segundos.c -o $(LIB_DIR)/$(LIB).so
#	$(CC) -c $(CFLAGS) $(SRC_DIR)/main.c -o $(OBJ_DIR)/main.o $(DFLAGS)
	$(CC) $(CFLAGS) $(SRC_DIR)/main.c -o $(BIN_DIR)/dinamico -L$(LIB_DIR) -lconv $(DFLAGS)
#	$(CC) $(OBJ_DIR)/main.o -o $(BIN_DIR)/dinamico -L$(LIB_DIR) -lconv
#$(LIB_DIR)/$(LIB).so -lm
#-L$(LIB_DIR) -lconv

#$(CC) -c $(CFLAGS) $(SRC_DIR)/main.c -o $(OBJ_DIR)/main.o $(DFLAGS)

debug: DFLAGS = -g
debug: clean all

clean:
	-rm -f $(OBJ_DIR)/*.o $(BIN_DIR)/* $(LIB_DIR)/*