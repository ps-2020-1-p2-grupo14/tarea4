#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <conv.h>

int main(int argc,char **argv){
	int op;
	while ((op = getopt(argc,argv,"d:s"))!=-1){
		switch (op)
		{
		case 'd':
			dias(atoi(argv[2]));
			break;		
		case 's':
			segundos(atoi(argv[2]));
			break;
		case ':':
			printf("Necesita agregar una opcion");
			break;
		case '?':  
       		    printf("Error en argumento %c, use -s para segundos o -d para dias \n", op);
                break;  
		default:
			return -1;
		}
	}
	
	return 0;
}
