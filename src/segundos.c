#include <stdio.h>
#include <stdlib.h>

#define HOUR_SECONDS 3600
#define MINUTE_SECONDS 60

void segundos(int seg){
	int hours, minutes, seconds;
	hours = (int ) seg/HOUR_SECONDS;		
	//start update input seconds
	seg = seg-hours*HOUR_SECONDS;
	//end update input seconds
	minutes = (int)seg/MINUTE_SECONDS;
	seconds = (int)seg-(minutes)*MINUTE_SECONDS;

	printf("%-9s	%-9s	%-9s	\n","Horas","Minutos","Segundos");
	printf("%-9d	%-9d	%-9d	\n",hours,minutes,seconds);
	
}
