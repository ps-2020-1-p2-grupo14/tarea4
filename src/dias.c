#include <stdio.h>
#include <stdlib.h>

#define YEAR_DAYS 360
//#define YEAR_MONTHS 12
#define MONTH_DAYS 30


void dias(int d)
{
    int year,month,day;
    
    year = (int)(d/YEAR_DAYS);
    d = d-year*YEAR_DAYS;
    month = (int)d/MONTH_DAYS ;
    day = (int)d-(month)*MONTH_DAYS;


    printf("%-9s    %-9s   %-9s \n","Años","Meses","Dias");
	printf("%-9d    %-9d   %-9d \n",year,month,day);

}
